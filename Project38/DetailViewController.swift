//
//  DetailViewController.swift
//  Project38
//
//  Created by Danni Brito on 4/18/20.
//  Copyright © 2020 Danni Brito. All rights reserved.
//

import WebKit
import UIKit

class DetailViewController: UIViewController, WKNavigationDelegate {
    
    var webView: WKWebView!
    
    var detailItem: Commit?

    @IBOutlet var detailLabel: UILabel!
    
    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        var url: URL!
        if let detail = self.detailItem {
            url = URL(string: "\(detail.url)")
            
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Commit 1/\(detail.author.commits.count)", style: .plain, target: self, action: #selector(showAuthorCommits))
            
        }
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
    }
    

    @objc func showAuthorCommits(){
        let vc = CommitsAuthorTableViewController()
        vc.author = detailItem?.author
        navigationController?.pushViewController(vc, animated: true)
    }

}
